import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
    {/* Botón Ir al Inicio */}
    <button type="button" className="btn btn-danger btn-floating btn-lg" id="btn-back-to-top">
      <i className="fas fa-arrow-up" />
    </button>
    {/* Navigation*/}
    <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div className="container">
        <a className="navbar-brand" href="#page-top"><img src="assets/img/Logo Juan Caicedo.png" /></a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i className="fas fa-bars ms-1" />
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li className="nav-item"><a className="nav-link" href="#services">Servicios</a></li>
            <li className="nav-item"><a className="nav-link" href="#portfolio">Certificaciones</a></li>
            <li className="nav-item"><a className="nav-link" href="#about">Mi experiencia</a></li>
            <li className="nav-item"><a className="nav-link" href="#team">Sobre Mí</a></li>
            <li className="nav-item"><a className="nav-link" href="#contact">Contáctame</a></li>
          </ul>
        </div>
      </div>
    </nav>
    {/* Masthead*/}
    <header className="masthead">
      <div className="container">
        <div className="masthead-subheading">¡Hola, Bienvenid@!</div>
        <div className="masthead-heading text-uppercase">A mi portafolio Web</div>
        <a className="btn btn-primary btn-xl text-uppercase" href="#services">Mi experiencia</a>
      </div>
    </header>
    {/* Services*/}
    <section className="page-section" id="services">
      <div className="container">
        <div className="text-center">
          <h2 className="section-heading text-uppercase">Servicios Ofertados</h2>
          <h3 className="section-subheading text-muted">Estos son los servicios que presto a las distintas compañias</h3>
        </div>
        <div className="row text-center">
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fas fa-circle fa-stack-2x text-primary" />
              <i className="fas fa-lock fa-stack-1x fa-inverse" />
            </span>
            <h4 className="my-3">Soprote Técnico</h4>
            <p className="text-muted">Te ayudo a tener un computador en óptimas condiciones mediante métodos de prevención tanto de hardware como de software.</p>
          </div>
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fas fa-circle fa-stack-2x text-primary" />
              <i className="fas fa-shopping-cart fa-stack-1x fa-inverse" />
            </span>
            <h4 className="my-3">E-Commerce</h4>
            <p className="text-muted">Diseño e implementación de E-Commerce mediante distintos Gestores de Contenido como lo son <br />Wordpress y Shopify.</p>
          </div>
          <div className="col-md-4">
            <span className="fa-stack fa-4x">
              <i className="fas fa-circle fa-stack-2x text-primary" />
              <i className="fas fa-laptop fa-stack-1x fa-inverse" />
            </span>
            <h4 className="my-3">Desarrollo Web</h4>
            <p className="text-muted">Desarrollo de páginas web y landing page dinámicas, con las cuales podrás tener precencia en internet y así llegar a más clientes, potencializando tu negocio digitalmente.</p>
          </div>
        </div>
      </div>
    </section>
    {/* Portfolio Grid*/}
    <section className="page-section bg-light" id="portfolio">
      <div className="container">
        <div className="text-center">
          <h2 className="section-heading text-uppercase">Certificaciones</h2>
          <h3 className="section-subheading text-muted">Te muestro un poco de las certificaciones que he realizado.</h3>
        </div>
        <div className="row">
          <div className="col-lg-4 col-sm-6 mb-4">
            {/* Portfolio item 1*/}
            <div className="portfolio-item">
              <a className="portfolio-link" data-bs-toggle="modal" href="#portfolioModal1">
                <div className="portfolio-hover">
                  <div className="portfolio-hover-content"><i className="fas fa-plus fa-3x" /></div>
                </div>
                <img className="img-fluid" src="assets/img/portfolio/1.jpg" alt="..." />
              </a>
              <div className="portfolio-caption">
                <div className="portfolio-caption-heading">Platzi</div>
                <div className="portfolio-caption-subheading text-muted">Marca Personal</div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-sm-6 mb-4">
            {/* Portfolio item 2*/}
            <div className="portfolio-item">
              <a className="portfolio-link" data-bs-toggle="modal" href="#portfolioModal2">
                <div className="portfolio-hover">
                  <div className="portfolio-hover-content"><i className="fas fa-plus fa-3x" /></div>
                </div>
                <img className="img-fluid" src="assets/img/portfolio/2.jpg" alt="..." />
              </a>
              <div className="portfolio-caption">
                <div className="portfolio-caption-heading">Converzzo</div>
                <div className="portfolio-caption-subheading text-muted">Curso Instagram y Facebook para negocios y Marcas Personales</div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-sm-6 mb-4">
            {/* Portfolio item 3*/}
            <div className="portfolio-item">
              <a className="portfolio-link" data-bs-toggle="modal" href="#portfolioModal3">
                <div className="portfolio-hover">
                  <div className="portfolio-hover-content"><i className="fas fa-plus fa-3x" /></div>
                </div>
                <img className="img-fluid" src="assets/img/portfolio/3.jpg" alt="..." />
              </a>
              <div className="portfolio-caption">
                <div className="portfolio-caption-heading">Platzi</div>
                <div className="portfolio-caption-subheading text-muted">Curso Básico de Python</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* About*/}
    <section className="page-section" id="about">
      <div className="container">
        <div className="text-center">
          <h2 className="section-heading text-uppercase">¡Mi experiencia laboral!</h2>
          <h3 className="section-subheading text-muted">Te cuento detalladamente de mi vida cómo profesional.</h3>
        </div>
        <ul className="timeline">
          <li>
            <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/2.jpg" alt="..." /></div>
            <div className="timeline-panel">
              <div className="timeline-heading">
                <h4>Marzo 2017 - Diciembre 2018</h4>
                <h4 className="subheading">DevPenguin S.A.S.</h4>
              </div>
              <div className="timeline-body"><p className="text-muted">Desarrollador de Páginas Web y Landing Page para clientes de la empresa mediante el lenguaje Ruby y framework Ruby On Rails. Adicionalmente adquirí experiencia en el Sistema Operativo Ubuntu y administración de repositorios en GitHub.</p></div>
            </div>
          </li>
          <li className="timeline-inverted">
            <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/1.jpg" alt="..." /></div>
            <div className="timeline-panel">
              <div className="timeline-heading">
                <h4>Marzo 2019 - Marzo 2022</h4>
                <h4 className="subheading">Comiagro S.A.</h4>
              </div>
              <div className="timeline-body"><p className="text-muted">Desempeño las siguientes funciones desempeñadas en la Gestión Tecnológica:<br />
                  • Realizar acciones de mantenimiento para brindar la solución de fallas en la red.<br />
                  • Realizar acciones de mantenimiento correctivo y preventivo del hardware y software de la empresa.<br />
                  • Coordinar el Inventario de activos relacionados con TI como Cámaras IP, Computadores de escritorio, Portátiles, Impresoras, Servidores, Switches, Routers, Ups y Periféricos.<br />
                  • Diseñar, implementar y realizar el mantenimiento de enlaces en telecomunicaciones VPN.<br />
                  • Configurar routers, switches y demás dispositivos relacionados con telecomunicaciones.<br />
                  • Manejar y administrar bases de datos.<br />
                  • Realizar campañas de concientización a los colaboradores sobre los diferentes deberes que tiene con el área de TI.<br />
                  • Realizar copias de seguridad de la información de cada usuario, garantizando que la misma se encuentre acorde con el sistema de seguridad de la información.<br />
                  • Resguardar el respaldo de datos en los diferentes medios de almacenamiento.<br />
                  • Soportar los Sistemas de Información (Contable y Operativo).<br />
                  • Administrar los Correos Electrónicos mediante de la GSuite de Google.<br />
                  • Establecer y gestionar los recursos requeridos para el mantenimiento del programa de seguridad de la información (recursos tecnológicos, humanos, formación, etc.)<br />
                  • Desarrollar e implementar procedimientos de seguridad detallados que fortalezcan la política de Seguridad de la Información de la organización.<br />
                  • Resguardar el software, sus licencias, y la documentación asociada a estos.<br />
                  • Diseñar y estructurar el Plan de Contingencia de Tecnología, realizando pruebas al mismo periódicamente.<br />
                  • Crear manuales, políticas, documentar casos de uso y soporte técnico.<br />
                  • Apoyo a la construcción del Sistema de Gestión y Seguridad de la Información. (ISO 27001).</p></div>
            </div>
          </li>
          <li>
            <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/3.jpg" alt="..." /></div>
            <div className="timeline-panel">
              <div className="timeline-heading">
                <h4>Año 2021</h4>
                <h4 className="subheading">PanExtra S.A.S.</h4>
              </div>
              <div className="timeline-body"><p className="text-muted">Realizar la instalación y actualización de hardware y software. Realizar el mantenimiento periódico preventivo de los dispositivos tecnológicos.</p></div>
            </div>
          </li>
          <li className="timeline-inverted">
            <div className="timeline-image">
              <h4>
                ¿Qué esperas?
                <br />
                Trabajemos
                <br />
                Juntos.
              </h4>
            </div>
          </li>
        </ul>
      </div>
    </section>
    {/* Team*/}
    <section className="page-section bg-light" id="team">
      <div className="container">
        <div className="text-center">
          <h2 className="section-heading text-uppercase">¡Conóceme un poco más!</h2>
          <h3 className="section-subheading text-muted">Aquí te cuento un poco sobre mi. ¡Tambien puedes visitar mis Redes Sociales!</h3>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="team-member">
              <img className="mx-auto rounded-circle" src="assets/img/team/1.jpg" alt="..." />
              <h4>Soy Juan Manuel Caicedo Castaño</h4>
              <p className="text-muted">Estudiante de Ingenieria de Sistemas y Telecomunicaciones - Universidad de Manziales.
                <br /> Técnico y Tecnólogo en Configuración de Sistemas de Información Comerciales Web - Universidad de Manizales.
                <br /> 
                <br /> 
              </p>
              <a className="btn btn-dark btn-social mx-2" href="https://www.instagram.com/_juancaicedo/" target="_blank" aria-label="Parveen Anand Instagram Profile"><i className="fab fa-instagram" /></a>
              <a className="btn btn-dark btn-social mx-2" href="https://github.com/juancaicedoum" target="_blank" aria-label="Parveen Anand Facebook Profile"><i className="fab fa-github" /></a>
              <a className="btn btn-dark btn-social mx-2" href="www.linkedin.com/in/juancaicedo-profile" target="_blank" aria-label="Parveen Anand LinkedIn Profile"><i className="fab fa-linkedin-in" /></a>
              <br /><br /><a className="btn btn-primary btn-xl text-uppercase" href="../../Desktop/PERSONAL/HV/HV - Juan Manuel Caicedo 2022.pdf" target="_blank">Descarga Mi Hoja de Vida</a>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-8 mx-auto text-center"><p className="large text-muted">Soy una persona dinámica y creativa, habituada al trabajo bajo presión. Tengo experiencia laboral de 4 años en diferentes ramas de la Ingenieria de Sistemas como los son el Desarrollo de Software y la Infraestructura Tecnológica. Cuento con la facilidad de entablar muy buenas relaciones interpersonales, por lo cual se me facilita el trabajo en equipo, lo cual además disfruto. Adquirí conocimientos en ejecución de proyectos mediante metodologías agiles. En tu empresa puedo demostrar mis habilidades aportando valor agregado a la misma y ayudando a la resolución de problemas y generación de nuevas ideas e implementación de nuevos proyectos al interior de ella, en el Área de Sistemas y el desarrollo de software.</p></div>
        </div>
      </div>
    </section>
    {/* Clients*/}
    <section id="team" className="page-section">
      <div className="container">
        <div className="text-center">
          <h2 className="section-heading text-uppercase">Vinculaciones laborales</h2>
          <h3 className="section-subheading text-muted">Te presento las empresas con las cuales he trabajado en el Área de Tecnología.</h3>
        </div>
        <div className="row no-gutters clients-wrap clearfix wow fadeInUp" style={{visibility: 'visible', animationName: 'fadeInUp'}}>
          <div className="col-lg-3 col-md-4 col-xs-6">
            <div className="client-logo"> <img src="assets/img/logos/comiagro.png" className="img-fluid" width="300px" height="65px" /> </div>
          </div>
          <div className="col-lg-3 col-md-4 col-xs-6">
            <div className="client-logo"> <img src="assets/img/logos/panextra.png" className="img-fluid" width="250px" height="65px" /> </div>
          </div>
          <div className="col-lg-3 col-md-4 col-xs-6">
            <div className="client-logo"> <img src="assets/img/logos/devpenguin.png" className="img-fluid" width="180px" height="65px" /> </div>
          </div>
          <div className="col-lg-3 col-md-4 col-xs-6">
            <div className="client-logo"> <img src="assets/img/logos/umanizales.png" className="img-fluid" width="200px" height="100px" /> </div>
          </div>
        </div>
      </div>
    </section>
    {/* Contact*/}
    <section className="page-section" id="contact">
      <div className="container">
        <div className="text-center">
          <h2 className="section-heading text-uppercase">Contáctame</h2>
          <h3 className="section-subheading text-muted">Trabajemos juntos para ayudarte a crecer tu negocio.</h3>
        </div>
        {/* * * * * * * * * * * * * * * **/}
        {/* * * SB Forms Contact Form * **/}
        {/* * * * * * * * * * * * * * * **/}
        {/* This form is pre-integrated with SB Forms.*/}
        {/* To make this form functional, sign up at*/}
        {/* https://startbootstrap.com/solution/contact-forms*/}
        {/* to get an API token!*/}
        <form id="contactForm" data-sb-form-api-token="API_TOKEN">
          <div className="row align-items-stretch mb-5">
            <div className="col-md-6">
              <div className="form-group">
                {/* Name input*/}
                <input className="form-control" id="name" type="text" placeholder="Nombre Completo *" data-sb-validations="required" />
                <div className="invalid-feedback" data-sb-feedback="name:required">Ingresa tu nombre.</div>
              </div>
              <div className="form-group">
                {/* Email address input*/}
                <input className="form-control" id="email" type="email" placeholder="Correo Electrónico *" data-sb-validations="required,email" />
                <div className="invalid-feedback" data-sb-feedback="email:required">Ingresa un Correo Electrónico</div>
                <div className="invalid-feedback" data-sb-feedback="email:email">El Correo Electrónico NO es válido.</div>
              </div>
              <div className="form-group mb-md-0">
                {/* Phone number input*/}
                <input className="form-control" id="phone" type="tel" placeholder="Celular *" data-sb-validations="required" />
                <div className="invalid-feedback" data-sb-feedback="phone:required">Ingresa tu número de teléfono.</div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group form-group-textarea mb-md-0">
                {/* Message input*/}
                <textarea className="form-control" id="message" placeholder="Escribe tu mensaje *" data-sb-validations="required" defaultValue={""} />
                <div className="invalid-feedback" data-sb-feedback="message:required">Ingresa tu mensaje.</div>
              </div>
            </div>
          </div>
          {/* Submit success message*/}
          {/**/}
          {/* This is what your users will see when the form*/}
          {/* has successfully submitted*/}
          <div className="d-none" id="submitSuccessMessage">
            <div className="text-center text-white mb-3">
              <div className="fw-bolder">Su mensaje fue enviado con éxito.</div>
              Me estaré comunicando lo más pronto posible contigo.<br /> ¡GRACIAS! por escribir.
              <br />
            </div>
          </div>
          {/* Submit error message*/}
          {/**/}
          {/* This is what your users will see when there is*/}
          {/* an error submitting the form*/}
          <div className="d-none" id="submitErrorMessage"><div className="text-center text-danger mb-3">¡Error enviando el mensaje!</div></div>
          {/* Submit Button*/}
          <div className="text-center"><button className="btn btn-primary btn-xl text-uppercase disabled" id="submitButton" type="submit">Envía tu mensaje</button></div>
        </form>
      </div>
    </section>
    {/* Footer*/}
    <footer className="footer py-4">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-4 text-lg-start">Copyright © Juan Caicedo 2022</div>
          <div className="col-lg-4 my-3 my-lg-0">
            <a className="btn btn-dark btn-social mx-2" href="https://www.instagram.com/_juancaicedo/" target="_blank" aria-label="Parveen Anand Instagram Profile"><i className="fab fa-instagram" /></a>
            <a className="btn btn-dark btn-social mx-2" href="https://github.com/juancaicedoum" target="_blank" aria-label="Parveen Anand Facebook Profile"><i className="fab fa-github" /></a>
            <a className="btn btn-dark btn-social mx-2" href="www.linkedin.com/in/juancaicedo-profile" target="_blank" aria-label="Parveen Anand LinkedIn Profile"><i className="fab fa-linkedin-in" /></a>
          </div>
          <div className="col-lg-4 text-lg-end">
            <a className="link-dark text-decoration-none me-3" href="#!">Politica de Privacidad</a>
            <a className="link-dark text-decoration-none" href="#!">Terminos y Condiciones</a>
          </div>
        </div>
      </div>
    </footer>
    {/* Portfolio Modals*/}
    {/* Portfolio item 1 modal popup*/}
    <div className="portfolio-modal modal fade" id="portfolioModal1" tabIndex={-1} role="dialog" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-8">
                <div className="modal-body">
                  {/* Project details*/}
                  <h2 className="text-uppercase">Curso de Marca Personal</h2>
                  <p>Tu marca personal es la puerta a tu carrera profesional. Decide cómo quieres que te perciban reclutadores, clientes y futuros aliados. Conoce todos los secretos detrás de crear y gestionar tu marca personal. Define y optimiza tus redes sociales, crea comunidades a través de una estrategia de contenido, y construye un portafolio que resalte en tu industria.</p>
                  <ul className="list-inline">
                    <li>
                      <strong>Certificado por:</strong>
                      Platzi
                    </li>
                    <li>
                      <strong>Año:</strong>
                      2018
                    </li>
                    <li>
                      <strong>Estado:</strong>
                      Certificado
                    </li>
                  </ul>
                  <button className="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                    <i className="fas fa-xmark me-1" />
                    Cerrar Ventana
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {/* Portfolio item 2 modal popup*/}
    <div className="portfolio-modal modal fade" id="portfolioModal2" tabIndex={-1} role="dialog" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-8">
                <div className="modal-body">
                  {/* Project details*/}
                  <h2 className="text-uppercase">Curso Instagram y Facebook para negocios y Marcas Personales</h2>
                  <p>Aquirí conocimientos en la Administración de redes sociales, diseño de campañas y contenido de valor, pauta publicitaria y creación de estrategias de marketing mediante metodologías ágiles y creación de Marca Personal y Corporativa.</p>
                  <ul className="list-inline">
                    <li>
                      <strong>Certificado por:</strong>
                      Converzzo
                    </li>
                    <li>
                      <strong>Año:</strong>
                      2020
                    </li>
                    <li>
                      <strong>Estado:</strong>
                      Certificado
                    </li>
                  </ul>
                  <button className="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                    <i className="fas fa-xmark me-1" />
                    Cerrar Ventana
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {/* Portfolio item 3 modal popup*/}
    <div className="portfolio-modal modal fade" id="portfolioModal3" tabIndex={-1} role="dialog" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-8">
                <div className="modal-body">
                  {/* Project details*/}
                  <h2 className="text-uppercase">Curso Básico de Python</h2>
                  <p>En el Curso Básico de Python comprendí e inicié nuevamente a programar desde cero con uno de los lenguajes de mayor crecimiento en el mundo: Python. Afiancé conocimientos sobre algoritmos y cómo se construye uno, domino las variables, funciones, estructuras de datos, los condicionales, ciclos y preparación de entornos virtuales
                    https://github.com/juancaicedoum/python_proyects.git</p>
                  <ul className="list-inline">
                    <li>
                      <strong>Certificado por:</strong>
                      Platzi
                    </li>
                    <li>
                      <strong>Año:</strong>
                      2018
                    </li>
                    <li>
                      <strong>Estado:</strong>
                      Certificado
                    </li>
                  </ul>
                  <button className="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                    <i className="fas fa-xmark me-1" />
                    Cerrar Ventana
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  );
}

export default App;
